const cors = require('cors')
const express = require("express")
const app = express()

app.use(express.json({limit:'50mb'}))

app.use(cors())

let questions = []

app.get("/retrieveQuestions", function (request, response) {
  response.json(questions)
})

app.post('/sendQuestions', function(request, response){
  questions.push(request.body)
  response.json([])
})

app.listen(8081, function () {
  console.log("Iniciado");
})
