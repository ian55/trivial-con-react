const oneSecond = 500

class PageObject {

  visitOurHomePage() {
    const url = 'localhost:8080'
    cy.visit(url)
  }

  waitUntilPageLoad() {
    cy.wait(oneSecond)
  }

  clickNewGame() {
    cy.get("button")
      .contains('Build New Game')
      .click()
  }



}

module.exports = { PageObject }