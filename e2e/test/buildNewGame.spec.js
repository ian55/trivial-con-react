import { PageObject } from './PageObject.js'

describe('Trivial:Build New Game', () => {
    let pageObject
    pageObject = new PageObject()
    
    beforeEach(() => {
        pageObject.visitOurHomePage()
        pageObject.waitUntilPageLoad()

    })

    it('allows users to enter the "send questions" option', () => {
        pageObject.clickNewGame()
        cy.get("#root").contains('Enter questions')

    })
    
    xit('Users send a question and three answers', () => {
        cy.get("button")
            .contains('Build New Game')
            .click()

        cy.get('[placeholder="enter"]').type("Funciona?")
        cy.get('[placeholder="answers"]')
            .type("YES")
        cy.wait(200)
        cy.get("[value=Add]").click()
        cy.wait(200)
        cy.get('[placeholder="answers"]')
            .last()
            .type("NO")
        cy.get("[value=Add]").click()
        cy.get('[placeholder="answers"]')
            .last()
            .type("maybe")
        cy.get("[value='2']").check()
        cy.get("button")
            .contains('Send question')
            .click()
        cy.wait(200)
        cy.get("button")
            .contains('Done')
            .click()
        cy.get("button")
            .contains('Join Game')
            .click()
        cy.get("#root").contains("Funciona?")

    })

})