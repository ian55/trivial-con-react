describe('Trivial', () => {
    beforeEach(() => {
        cy.visit('/')
        cy.wait(200)
    })

    xit('check if can send a right answer', () => {
        cy.get('[type="radio"]')
            .check('B')

        cy.get('button')
            .click()

        cy.get('#root')
            .contains('You were right!')
    })

    xit('check if can send a wrong answer', () => {
        cy.get('[type="radio"]')
        .check('A')

        cy.get('button')
        .click()

        cy.get('#root')
        .contains('You were wrong!')
    })

    xit('send answer after time out', () => {
        cy.get('[type="radio"]')
        .check('A')

        cy.wait(10500)

        cy.get('#root')
        .contains('You were wrong!')
    })
})