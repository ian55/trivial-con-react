import React from "react";

import Button from "./components/Button"
import JoinGame from "./components/JoinGame"
import BuildNewGame from "./components/BuildNewGame"


class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      view: "Home",
    }
  }

  changeView = (newView) => {
    this.setState({
      view: newView,
    })
  }

  createView() {
    switch (this.state.view) {
      case "Home":
        return (
          <div>
            <div>
              <h1>Trivial Game</h1>
            </div>
            <Button
              handleClick={() => this.changeView("JoinGame")}
              buttonName="Join Game"
            />
            <Button
              handleClick={() => this.changeView("BuildGame")}
              buttonName="Build New Game"
            />
            <br />
            <p>Instructions:</p>
          </div>
        );
        break;
      case "BuildGame":
        return <BuildNewGame changeView={this.changeView}/>
        break;
      case "JoinGame":
        return <JoinGame />
        break;
      default:
        break;
    }
  }

  render() {
    let vista = this.createView()

    return <div>{vista}</div>
  }
}


export default Home