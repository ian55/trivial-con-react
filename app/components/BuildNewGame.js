import React from "react"

import Button from "./Button"
import Header from "./Header"

class BuildNewGame extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      question: "",
      answers: [""],
      correctAnswer: "",
    }
  }

  handleSubmit = (event) => {
    event.preventDefault()
    if (this.state.answers.length >= 4){
      return alert('The maximun number of answers is four')
    }
    this.setState({ answers: this.state.answers.concat('') })
  }

  handleAnswerChange = (event, index) => {
    let answerList = [...this.state.answers]
    answerList[index] = event.target.value
    this.setState({ answers: answerList })
  }

  handleQuestionChange = (event) => {
    this.setState({ question: event.target.value })
  }

  handleCheckedChange = (event) => {
    this.setState({ correctAnswer: parseInt(event.target.value) })
  }

  futureFetch = () => {

    if (this.state.question === ""){
      return alert('You need to write a question')
    }
   
    if (this.state.answers.filter(element => element != "").length < 2){
      return alert('You need at least two answers')
    }

    if (this.state.correctAnswer === "") {
      return alert('You need to choose a correct answer')
    }

    let answersLowercase = this.state.answers.map(element => element.toLowerCase())
    if ([...new Set(answersLowercase)].length != answersLowercase.length) {
      return alert("Don't write duplicated answers")
    }

    const translator = ['A', 'B', 'C', 'D']

    let ourQuestion = {
      question: this.state.question,
      answers: this.state.answers,
      correctAnswer: translator[this.state.correctAnswer]
    }

    fetch("http://localhost:8081/sendQuestions", {
      method: 'POST',
      body: JSON.stringify(ourQuestion),
      headers: {
        'Accept':'application/json',
        'Content-Type': 'application/json'
      }
    }
    ).then(response=>response.json())

    this.setState({answers: [""], question: "", correctAnswer: ""})
  }

  render() {
    return (
      <div>
        <Header />
        <h1>Enter questions</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Question 1
            <br />
            <input type="text" value={this.state.question} placeholder='enter' size='60' onChange={this.handleQuestionChange} />
            <br />
            <br />

            {this.state.answers.map((_, index) => {
              return (
                <div>
                  <input name='choice' type="radio" value={index} defaultChecked={false} onChange={this.handleCheckedChange} />
                  <input type="text" placeholder='answers' onChange={(event) => this.handleAnswerChange(event, index)} value={this.state.answers[index]} />
                </div>
              )
            })}
          </label>
          <input type="submit" value="Add" />
        </form>
        <br />
        <Button handleClick={() => this.futureFetch()} buttonName="Send question" />
        <Button handleClick={() => this.props.changeView('Home')}buttonName='Done'/>
      </div>
    )
  }
}

export default BuildNewGame
