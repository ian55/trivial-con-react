import React from "react"

class Answers extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            timer: 10, 
        }
        this.countDown = this.countDown.bind(this) 
        this.handleChange = this.handleChange.bind(this)
    }
    
    componentDidMount(){
        this.countDown()
    }
    
    countDown(){
        if (this.state.timer > 0){
            setTimeout(() => { 
                this.setState({timer : this.state.timer -1}) 
                this.countDown()
            }, 1000)
            return
        }
        
        this.props.runOutOfTime()
        
    }

    handleChange(event){
        this.props.saveChoosedAnswer(event.target.value) 
    }

    printAnswers(){
        return this.props.answersList.map((answer) => 
        <span key={answer}>
            <input 
                onChange={e => this.handleChange(e)} 
                type='radio' 
                name='answerOption' 
                value={answer}
            /> 
            {answer}
            <br/>
        </span>)
    }

    render(){
        let answerOptions = this.printAnswers()
        
        return (
            <div id='answers'>
                {answerOptions}
                <h2>Time: {this.state.timer}</h2>
            </div>
        )
    }  
}

export default Answers
