import React from "react"

import Button from "./Button"
import Header from "./Header"
import Questions from "./Questions"
import Answers from "./Answers"

class JoinGame extends React.Component {
  constructor() {
    super()
    this.state = {
      question: '',
      answers: [],
      correctAnswer: '',
      choosedAnswer: "",
      resultScreen: false,
      resultToAnswer: "",
      points: [],
      questionIndex: 0,
    }
  }

  componentDidMount(){

    fetch("http://localhost:8081/retrieveQuestions")
      .then(response => {
        return response.json()
      })
      .then(obj => {
        this.setState({question : obj[this.state.questionIndex].question})
        this.setState({answers : obj[this.state.questionIndex].answers})
        this.setState({correctAnswer : obj[this.state.questionIndex].correctAnswer})
      })
  }

  runOutOfTime = () => {
    const previousPoints = this.state.points;
    if (this.state.correctAnswer === this.state.choosedAnswer) {
      this.setState({ resultToAnswer: "You were right!" });
      this.setState({ points: previousPoints.concat([1]) });
    } else {
      this.setState({ points: previousPoints.concat([0]) });
      this.setState({ resultToAnswer: "You were wrong!" });
    }
    this.setState({ resultScreen: true });
  }

  saveChoosedAnswer = (choosedAnswer) => {
    this.setState({ choosedAnswer: choosedAnswer });
  }

  render() {
    if (this.state.resultScreen === true) {
      return (
        <div>
          <Header />
          <Questions question={this.state.question} />
          {this.state.answers} <br />
          {this.state.resultToAnswer} <br />
          {this.state.correctAnswer === this.state.choosedAnswer
            ? ""
            : `The right answer was: ${this.state.correctAnswer}`}{" "}
          <br />
          Points:{this.state.points}
        </div>
      );
    }

    return (
      <div>
        <Header />
        <Questions question={this.state.question} />
        <Answers
          answersList={this.state.answers}
          runOutOfTime={this.runOutOfTime}
          saveChoosedAnswer={this.saveChoosedAnswer}
        />
        <Button handleClick={this.runOutOfTime} buttonName="Send" />
      </div>
    );
  }
}

export default JoinGame
