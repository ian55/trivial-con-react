import React from "react"
import { render, fireEvent } from "@testing-library/react"
import Answers from "../components/Answers.js"

describe("Answers", () => {
  it("shows players the answers", () => {
    const { getByText} = render(<Answers answersList={["Sí", "No"]} />)

    getByText(/Sí/i)
    getByText(/No/i)
  })
})